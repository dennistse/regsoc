var mysql = require('../db');

exports.IndexInit = function(req,res,callback){
	mysql.query('select a.overall,b.soc_code from reg_soc_form as a, reg_soc_doc as b where a.regsoc_id=b.record_id;',function(err,obj,fields){
		if(err){
			console.log(err);
		}else{
			mysql.query('SELECT * FROM Soc_info order by soc_type',function(err,rows,fields,res){
				if(err){
					console.log('\n \033[1;31m Admin Regsoc Error \033[0m\n');
					console.log(err);
					callback(' ','err');
				}else{
					console.log("\n \033[1;32m Get all Soc_info \033[0m\n");
					if(rows.length == 0){
						console.log('\n \033[1;33m  all Soc_info emtpy \033[0m\n');
						callback(' ',' ');
						return;
					}
					callback(rows,obj);
				}
			});
		}
	});

}


exports.RegSocFormInit = function(req,res,callback){
	mysql.query('SELECT * FROM Reg_Soc_form where admin_user_id	= "'+req.signedCookies.userid+'" and regsoc_id=(select record_id from Reg_Soc_Doc where soc_code="'+req.query["soc_code"]+'" order by record_id asc limit 1)',function(err,rows,fields,res){
		if(err){
			console.log('\n \033[1;31m RegSocFormInitError \033[0m\n');
			console.log(err);
			callback(' ');
		}else{
			console.log(rows);
			console.log("\n \033[1;32m RegSocFormInit \033[0m\n");
			if(rows.length == 0){
				console.log('\n \033[1;33m  RegSocFormInit emtpy \033[0m\n');
				callback(' ',' ');
				return;
			}
			callback(rows);
		}
	});
}

exports.RegSocFormSubmit = function(req,res,callback){
	mysql.query('select * from Reg_Soc_Doc  where soc_code = "'+req.body['soc_code']+'" order by record_id asc limit 1',function(err,rows,fields,res){
		if(err){
			console.log(err);
				callback(false);
			return;
		}else{
		mysql.query('replace into Reg_Soc_form(id,admin_user_id,regsoc_id,overall,form1,form2,form3A,form3B,form3C,form3D,form4,note) values ("'+req.body['soc_code']+'","'+req.signedCookies.userid+'","'+rows[0]['record_id']+'","'+req.body["overall"]+'","'+req.body["form1"]+'","'+req.body["form2"]+'","'+req.body["form3A"]+'","'+req.body["form3B"]+'","'+req.body["form3C"]+'","'+req.body["form3D"]+'","'+req.body["form4"]+'","'+req.body["note"]+'" )',function(err,rows,fields,res){
			if(err){
				console.log('\n \033[1;31m RegSocFormSubmit Error \033[0m\n');
				console.log(err);
				callback(false);
			}else{
				console.log("\n \033[1;32m RegSocFormSubmit  \033[0m\n");
				callback(true);
				if(rows.length == 0){
					console.log('\n \033[1;33m  RegSocFormSubmit emtpy \033[0m\n');
					
					return;
				}
			}
		});
	}
	});
}