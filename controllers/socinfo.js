var SocInfo  = require('../models/socinfo');
var CheckLogin  = require('../models/checkLogin');
var User 	= require('../models/user');


exports.EnterSocInfo = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		SocInfo.index(req,res, function(list,process){ 
			SocInfo.socDetail(req,res,function(detail_list){
				res.render( 'socinfo',{
					soc_detail : detail_list,
					soc_list : list,
					IsAdmin : isAdmin,
					Process : process,
					loginStatus : isLogin
				});
			});
		});	
	});
};


