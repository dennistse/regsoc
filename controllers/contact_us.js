var Contact  = require('../models/contact_us');
var User 	= require('../models/user');


exports.EnterContact = function(req, res){

	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(isLogin){
			Contact.ContactInit(req,res,function(name){
				res.render('contact_us',{
					user_name : name,
					IsAdmin : isAdmin,
					loginStatus : isLogin
				});
			});
		}else{
			return res.redirect('/');
		}
		
	});

	
};