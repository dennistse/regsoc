var Form4  = require('../models/form4');
var CheckLogin  = require('../models/checkLogin');

exports.Enterform4 = function(req, res){
	console.log(req.signedCookies.userid);
	var isAdmin = CheckLogin.IsAdmin(req,res);
	var isLogin = CheckLogin.checkLoginStatus(req,res);
	Form4.Form4Init(req,res,function(soc){
		res.render('form4',{
			Soc :soc,
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	});
};


exports.submit_form4 =  function(req, res) {
	//upload file
	var isAdmin = CheckLogin.IsAdmin(req,res);
	var isLogin = CheckLogin.checkLoginStatus(req,res);

	Form4.submitForm4(req,res,function(success){
		if(success){
			res.render( 'submit_form4',{
				upload_status : '成功',
				upload_message : '成功更新資料',
				return_path : 'form4',
				IsAdmin : isAdmin,
				loginStatus : isLogin
			});
		}else{
			return res.redirect( 'form4');
		}
	});
};

exports.adminform4 = function(req,res){
	Form4.AdminInit(req,res,function(soc){
		res.render('admin/regsoc_form4',{
			Soc : soc
		});
	});
}