var admin  = require('../models/adminRegsoc.js');
var User 	= require('../models/user');
var Form1  = require('../models/form1');

exports.regsoc = function(req, res){
	
	if(req.query["soc_code"]!= undefined){
		soc_code  = req.query["soc_code"];
	}else{
		soc_code = 1;
		console.log('req.query["soc_code"] Not Set');
	}
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		Form1.AdminInit(req,res,function(soc){
			res.render( 'regsoc1',{
				IsAdmin : isAdmin,
				loginStatus : isLogin,
				id : req.query["soc_code"],
				Soc: soc
			});
		});
	});
};


exports.RegSocIndex = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin ){
			console.log('not login');
			return res.redirect('login');
		}else{
			admin.IndexInit(req,res,function(socs,obj){
				console.log("*%j, %j",obj,socs);
				res.render('regsoc',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					status : obj,
					socs : socs
				});
			});
		}
	});
}

exports.RegSocForm = function(req,res){
		User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin ){
			console.log('not login');
			return res.redirect('login');
		}else{
			admin.RegSocFormInit(req,res,function(socs){
				res.render('admin/regsocform',{
					IsAdmin : isAdmin,
					loginStatus : isLogin,
					info : socs,
					soc_code: req.query['soc_code']
				});
			});
		}
	});
}

exports.submit_RegSocForm =  function(req, res) {
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		if(!isLogin ){
			console.log('not login');
			return res.redirect('login');
		}else{
	admin.RegSocFormSubmit(req,res,function(success){
		if(success){
			res.render( 'submit_form1',{
				upload_status : '成功',
				upload_message : '成功更新資料',
				return_path : 'regsocform?soc_code=1',
				IsAdmin : isAdmin,
				loginStatus : isLogin
			});
		}else{
			//return res.redirect( 'form1');
		}
	});
}
});
}
/*
exports.adminform1 = function(req,res){
	if(req.query["soc_code"]!= undefined){
		soc_code  = req.query["soc_code"];
	}else{
		soc_code = 1;
		console.log('req.query["soc_code"] Not Set');
	}
	adminRegsoc.AdminInit(req,res,soc_code,function(soc){
		res.render('regsoc',{
			Soc : soc,
		});
	});
}
*/