//var mysql = require('../db');
//var Form2 = require('../models/form3');
var User = require('../models/user');
var nodemailer = require("nodemailer");

var smtpTransport = nodemailer.createTransport('SMTP',{
	service: 'gmail',
	auth: {
		user: 'regsocnet@gmail.com',
		pass: 'regsoc321'
	}
});


exports.Enteremail = function(req, res){
	User.checkLoginStatus(req,res, function(isLogin,isAdmin){
		res.render('email',{
			IsAdmin : isAdmin,
			loginStatus : isLogin
		});
	});
};


exports.Sendemail = function(req,res){
	console.log('to: '+req.query.to);
	var mailOptions={
		to : req.query.to,
		subject : 'Email Notification from RegSoc Net',
		text : 'Your application is accepted.'
	}
	console.log(mailOptions);
	smtpTransport.sendMail(mailOptions, function(error, response){
		if(error){
			console.log(error);
			res.end("error");
		}else{
			console.log("Message sent: " + response.message);
			res.end("sent");
		}
	});
};

exports.SendFemail = function(req,res){
	console.log('to: '+req.query.to);
	var mailOptions={
		to : req.query.to,
		subject : 'Email Notification from RegSoc Net',
		text : 'Your application is failed.'
	}
	console.log(mailOptions);
	smtpTransport.sendMail(mailOptions, function(error, response){
		if(error){
			console.log(error);
			res.end("error");
		}else{
			console.log("Message sent: " + response.message);
			res.end("sent");
		}
	});
};