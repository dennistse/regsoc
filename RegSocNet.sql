-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: regSocNet
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AGM_Motion`
--

DROP TABLE IF EXISTS `AGM_Motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AGM_Motion` (
  `agm_ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `motion_ref_no` int(11) NOT NULL,
  `datetime` date DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `chairman_name` varchar(50) DEFAULT NULL,
  `chairman_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`agm_ref_no`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AGM_Motion`
--

LOCK TABLES `AGM_Motion` WRITE;
/*!40000 ALTER TABLE `AGM_Motion` DISABLE KEYS */;
INSERT INTO `AGM_Motion` VALUES (1,1,'1999-01-01','1999/1/1','12323','4'),(2,2,'2000-12-01','2000/2/1','ASD','12'),(3,3,'1999-01-01','1999/1/1','saf','sdf'),(4,4,'2000-12-01','2000/2/1','tttt','123o'),(5,5,'2000-12-01','2000/12/1','2000/12/1','2000/12/1'),(6,6,'2000-12-01','2000/12/1','2000/12/1','2000/12/1'),(7,7,'2000-01-01','2000/1/1','2000/1/1','2000/1/1'),(8,8,'1999-01-01','1999/1/1','1999/1/1','1999/1/1'),(9,9,'1999-01-01','1999/1/1','1999/1/1','1999/1/1'),(10,10,'1999-01-01','1999/1/1','1999/1/1','1999/1/1'),(11,11,'1999-01-01','1999/1/1','1999/1/1','1999/1/1'),(12,12,'1999-01-01','1999/1/1','1999/1/1','1999/1/1'),(13,13,'2000-01-01','2000/12/1','2000/1/1','2000/1/1'),(14,15,'2000-12-01','2000/12/1','2000/1/1','2000/1/1'),(15,16,'2000-11-01','2000/12/1','2000/1/1','2000/1/1'),(16,17,'2000-09-01','2000/12/1','2000/1/1','2000/1/1'),(17,18,'2000-08-01','2000/12/1','2000/1/1','2000/1/1'),(18,19,'2000-07-01','2000/12/1','2000/1/1','2000/1/1'),(19,20,'2000-06-01','2000/12/1','2000/1/1','2000/1/1'),(20,22,'2000-05-01','2000/12/1','2000/1/1','2000/1/1'),(21,23,'2000-04-01','2000/12/1','2000/1/1','2000/1/1'),(22,24,'2000-03-01','2000/12/1','2000/1/1','2000/1/1'),(23,25,'2000-02-01','2000/12/1','2000/1/1','2000/1/1'),(24,26,'2000-01-01','2000/12/1','2000/1/1','2000/1/1'),(25,27,'2032-01-01','2000/12/1','2000/1/1','2000/1/1'),(26,28,'2013-02-02','2000/12/1','2000/1/1','2000/1/1'),(27,29,'2013-02-02','2000/12/1','2000/1/1','2000/1/1'),(28,33,'2013-02-02','2000/12/1','2000/1/1','2000/1/1'),(29,34,'2013-02-02','2000/12/1','2000/1/1','2000/1/1'),(30,35,'2013-02-02','2000/12/1','2000/1/1','2000/1/1'),(31,37,'1983-12-03','1983/12/3','1983/12/3','v');
/*!40000 ALTER TABLE `AGM_Motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Exco_Member`
--

DROP TABLE IF EXISTS `Exco_Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Exco_Member` (
  `exco_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `exco_id` int(11) DEFAULT NULL,
  `exco_member_post` varchar(50) DEFAULT NULL,
  `exco_member_Cname` varchar(30) DEFAULT NULL,
  `exco_member_Ename` varchar(50) DEFAULT NULL,
  `exco_member_sid` varchar(10) DEFAULT NULL,
  `exco_member_college` varchar(20) DEFAULT NULL,
  `exco_member_major` varchar(50) DEFAULT NULL,
  `exco_member_year_of_study` varchar(1) DEFAULT NULL,
  `exco_member_phone` varchar(8) DEFAULT NULL,
  `exco_member_email` varchar(50) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`exco_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Exco_Member`
--

LOCK TABLES `Exco_Member` WRITE;
/*!40000 ALTER TABLE `Exco_Member` DISABLE KEYS */;
INSERT INTO `Exco_Member` VALUES (1,1,'會長','陳大文','Chan Tai Man','1155011111','CC','CS','2','51672828',NULL,'2015-04-09 10:30:31'),(2,1,'asd','︱翁︱剃','asd','13123','CC','sdf','3','123',NULL,'2015-04-09 10:30:31'),(3,2,'chairman','少鏽考','GG','13453423','CC','ENGG','2','23452354',NULL,'2015-04-06 11:02:39'),(4,1,'sdf','sdf','sdfd','123','CC','sdf','2','423',NULL,'2015-04-09 10:30:31'),(5,26,'asd','asd','asd','123','CC','cs','2','123',NULL,'2015-04-09 09:57:16'),(6,26,'asdjh','ajhsd','jhsfd','123','CC','cs','2','123',NULL,'2015-04-09 09:57:16');
/*!40000 ALTER TABLE `Exco_Member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IM`
--

DROP TABLE IF EXISTS `IM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IM` (
  `chatwith` varchar(10) NOT NULL,
  `person` varchar(10) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IM`
--

LOCK TABLES `IM` WRITE;
/*!40000 ALTER TABLE `IM` DISABLE KEYS */;
INSERT INTO `IM` VALUES ('1','1','14','2015-04-09 09:36:13'),('1','1','Hi','2015-04-09 09:39:13'),('1','admin','hi 1','2015-04-09 09:39:58'),('4','4','Test','2015-04-09 09:40:45'),('1','1','Hihi','2015-04-09 10:13:12'),('1','admin','hi','2015-04-09 10:13:27'),('1','1','Thz','2015-04-09 10:37:19'),('1','admin','You\'re welcome','2015-04-09 10:37:35'),('1','1','Thanks','2015-04-09 10:37:57');
/*!40000 ALTER TABLE `IM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Motion`
--

DROP TABLE IF EXISTS `Motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Motion` (
  `ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(25) NOT NULL,
  `status` varchar(30) DEFAULT NULL,
  `no_of_member` int(11) DEFAULT NULL,
  `legal_mini_no` int(11) DEFAULT NULL,
  `no_of_vote` int(11) DEFAULT NULL,
  `no_of_agr` int(11) DEFAULT NULL,
  `no_of_opp` int(11) DEFAULT NULL,
  `no_of_abs` int(11) DEFAULT NULL,
  `no_of_inv` int(11) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ref_no`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Motion`
--

LOCK TABLES `Motion` WRITE;
/*!40000 ALTER TABLE `Motion` DISABLE KEYS */;
INSERT INTO `Motion` VALUES (1,'meeting','pass',3,2,3,3,2,0,0,'2015-04-08 20:16:43'),(2,'meeting','pass',3,2,2,3,2,1,5,'2015-04-08 20:22:27'),(3,'meeting','pass',2,2,2,2,0,0,0,'2015-04-08 20:27:52'),(4,'meeting','pass',1,9,9,1,1,3,2,'2015-04-08 20:30:24'),(5,'meeting','pass',1,3,2,1,1,1,1,'2015-04-08 20:31:32'),(6,'meeting','pass',1,1,1,1,2,3,2,'2015-04-08 20:32:43'),(7,'meeting','pass',1,1,2,2,2,2,2,'2015-04-08 20:33:45'),(8,'meeting','pass',1,1,1,1,2,1,3,'2015-04-08 20:51:53'),(9,'meeting','pass',1,1,1,1,2,1,3,'2015-04-08 20:51:53'),(10,'meeting','pass',1,1,1,1,2,1,3,'2015-04-08 20:52:48'),(11,'meeting','pass',1,1,1,1,2,1,3,'2015-04-08 20:53:44'),(12,'meeting','pass',1,1,3,1,2,3,4,'2015-04-08 20:54:49'),(13,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 21:04:37'),(14,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:00:47'),(15,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:08:17'),(16,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:08:59'),(17,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:09:21'),(18,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:09:40'),(19,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:12:51'),(20,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:19'),(21,'other','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:23'),(22,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:29'),(23,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:34'),(24,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:38'),(25,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:43'),(26,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:13:47'),(27,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:16:20'),(28,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:16:29'),(29,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:16:42'),(30,'other','pass',2,2,3,3,2,2,2,'2015-04-08 22:27:02'),(31,'other','pass',2,2,3,3,2,2,2,'2015-04-08 22:31:00'),(32,'other','pass',2,2,3,3,2,2,2,'2015-04-08 22:31:20'),(33,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:32:29'),(34,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:33:19'),(35,'meeting','pass',2,2,3,3,2,2,2,'2015-04-08 22:35:00'),(36,'vote','fail',123,73,119,43,2,21,24,'2015-04-08 22:39:09'),(37,'meeting','pass',1,1,1,1,1,1,1,'2015-04-08 23:55:23'),(38,'vote','pass',12,32,12,31,123,112,23,'2015-04-09 00:41:44'),(39,'vote','pass',200,100,150,120,20,8,2,'2015-04-09 10:32:11'),(40,'vote','pass',200,100,150,120,20,8,2,'2015-04-09 10:32:29');
/*!40000 ALTER TABLE `Motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Notice`
--

DROP TABLE IF EXISTS `Notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notice_content` text,
  `post_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Notice`
--

LOCK TABLES `Notice` WRITE;
/*!40000 ALTER TABLE `Notice` DISABLE KEYS */;
INSERT INTO `Notice` VALUES (1,'Title 1','1 notice_content123','2015-03-13 02:20:15','2015-04-09 10:10:45'),(2,'title 2','2','2015-03-13 02:20:19','0000-00-00 00:00:00'),(3,'title 3','3','2015-03-13 02:20:23','0000-00-00 00:00:00'),(4,'Title 4','4notice_content','2015-03-13 02:20:26','2015-03-15 16:03:25'),(6,'jdkfksdjfsds','sdfsjdhfksdj','2015-03-16 04:26:13','0000-00-00 00:00:00'),(7,'test','ser','2015-04-09 10:15:26','0000-00-00 00:00:00'),(8,'qwe','qsasd','2015-04-09 10:16:01','0000-00-00 00:00:00'),(9,'0000','123','2015-04-09 10:16:19','0000-00-00 00:00:00'),(10,'Hello World','Hello World','2015-04-09 10:17:09','0000-00-00 00:00:00'),(11,'happy demo 310','happy demo','2015-04-09 10:17:39','0000-00-00 00:00:00'),(12,'happy demo 310','happy demo ','2015-04-09 10:34:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `Notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reg_Soc_Doc`
--

DROP TABLE IF EXISTS `Reg_Soc_Doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reg_Soc_Doc` (
  `record_id` int(11) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  `exco_elect_ref_no` int(11) DEFAULT NULL,
  `year_plan_ref_no` int(11) DEFAULT NULL,
  `budget_ref_no` int(11) DEFAULT NULL,
  `report_ref_no` int(11) DEFAULT NULL,
  `financial_report_ref_no` int(11) DEFAULT NULL,
  `year_plan_url` varchar(255) DEFAULT NULL,
  `budget_url` varchar(255) DEFAULT NULL,
  `report_url` varchar(255) DEFAULT NULL,
  `financial_url` varchar(255) DEFAULT NULL,
  `note_by_exco` text,
  `note_by_committee` text,
  `reg_soc_status` varchar(30) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `1st_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `2nd_process_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reg_Soc_Doc`
--

LOCK TABLES `Reg_Soc_Doc` WRITE;
/*!40000 ALTER TABLE `Reg_Soc_Doc` DISABLE KEYS */;
INSERT INTO `Reg_Soc_Doc` VALUES (1,'1',NULL,38,NULL,40,NULL,'/soc/學生團體財務須知.pdf',NULL,'/soc/chapter3_part2.pdf',NULL,NULL,NULL,NULL,'2015-04-25 12:53:27','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `Reg_Soc_Doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reg_Soc_Form`
--

DROP TABLE IF EXISTS `Reg_Soc_Form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reg_Soc_Form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form1` varchar(255) COLLATE utf8_bin NOT NULL,
  `form2` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3a` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3b` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3c` varchar(255) COLLATE utf8_bin NOT NULL,
  `form3d` varchar(255) COLLATE utf8_bin NOT NULL,
  `form4` varchar(255) COLLATE utf8_bin NOT NULL,
  `note` text COLLATE utf8_bin NOT NULL,
  `admin_user_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `regsoc_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `overall` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reg_Soc_Form`
--

LOCK TABLES `Reg_Soc_Form` WRITE;
/*!40000 ALTER TABLE `Reg_Soc_Form` DISABLE KEYS */;
INSERT INTO `Reg_Soc_Form` VALUES (1,'on','on','on','undefined','on','undefined','undefined','','admin@gmail.com','1','2015-04-09 10:33:33','on');
/*!40000 ALTER TABLE `Reg_Soc_Form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `user_id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `soc_code` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'admin@gmail.com','abcd1234','admin'),(2,'user@gmail.com','abcd1234','1'),(3,'user1@gmail.com','abcd1234','2'),(4,'test','test','3'),(5,'narep51@gmail.com','test','4');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vote_Motion`
--

DROP TABLE IF EXISTS `Vote_Motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vote_Motion` (
  `vote_ref_no` int(11) NOT NULL AUTO_INCREMENT,
  `motion_ref_no` int(11) NOT NULL,
  `poll_start_date` date DEFAULT NULL,
  `poll_end_date` date DEFAULT NULL,
  `poll_detail` varchar(255) DEFAULT NULL,
  `poll_method` varchar(50) DEFAULT NULL,
  `ballot_temp` varchar(255) DEFAULT NULL,
  `elect_commitee` varchar(255) DEFAULT NULL,
  `scrutineer_name` varchar(50) DEFAULT NULL,
  `scrutineer_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vote_ref_no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vote_Motion`
--

LOCK TABLES `Vote_Motion` WRITE;
/*!40000 ALTER TABLE `Vote_Motion` DISABLE KEYS */;
INSERT INTO `Vote_Motion` VALUES (1,36,'2013-02-02','2014-05-01','需列明日期、時間、地點','投票方式','/soc/新亞學生會會章(20150227).pdf','asdq212','123','123'),(2,38,'2014-01-01','2015-01-01','asdkjfogsoeritu','sdifuoi','/soc/tuto06.pdf',' qwe13 osii3sdf ','123123','25345'),(3,39,'2014-03-01','2014-03-07','2014/3/1 - 2014/3/7 12:00 – 18:00 SHB 5 Floor','票站',NULL,'李大明 Lee Tai Ming ​1155011122 ​Year 2​cc ​computer science ','何琪琪','Ho Ki Ki'),(4,40,'2014-03-01','2014-03-07','2014/3/1 - 2014/3/7 12:00 – 18:00 SHB 5 Floor','票站',NULL,'李大明 Lee Tai Ming ​1155011122 ​Year 2​cc ​computer science ','何琪琪','Ho Ki Ki');
/*!40000 ALTER TABLE `Vote_Motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exco_info`
--

DROP TABLE IF EXISTS `exco_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exco_info` (
  `exco_id` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) DEFAULT NULL,
  `exco_name` varchar(255) NOT NULL,
  `exco_start_date` varchar(255) DEFAULT NULL,
  `exco_end_date` varchar(255) DEFAULT NULL,
  `soc_code` varchar(11) NOT NULL,
  PRIMARY KEY (`exco_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exco_info`
--

LOCK TABLES `exco_info` WRITE;
/*!40000 ALTER TABLE `exco_info` DISABLE KEYS */;
INSERT INTO `exco_info` VALUES (1,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1'),(22,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1'),(23,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1'),(24,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1'),(25,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1'),(26,2015,'NAP','01/04/2015','31/03/2016','4'),(27,2015,'NAP','01/04/2015','31/03/2016','4'),(28,2015,'NAP','01/04/2015','31/03/2016','4'),(29,2015,'幹事會內閣名稱','2015-01-01','2015-12-12','1');
/*!40000 ALTER TABLE `exco_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soc_info`
--

DROP TABLE IF EXISTS `soc_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soc_info` (
  `soc_code` varchar(10) NOT NULL,
  `soc_name` varchar(255) DEFAULT NULL,
  `constitution_ref_no` varchar(255) DEFAULT NULL,
  `soc_type` varchar(10) DEFAULT NULL,
  `note_by_committee` varchar(255) DEFAULT NULL,
  `submit_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `num_of_mem` int(11) DEFAULT NULL,
  `mem_list_url` varchar(255) DEFAULT NULL,
  `exco_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`soc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soc_info`
--

LOCK TABLES `soc_info` WRITE;
/*!40000 ALTER TABLE `soc_info` DISABLE KEYS */;
INSERT INTO `soc_info` VALUES ('1','計算機科學系會',NULL,'dept',NULL,'2015-04-09 10:30:31',29,NULL,1),('2','工程學院院會',NULL,'facu',NULL,'2015-04-06 10:17:16',40,NULL,2),('4','新亞學生會','/soc/constitution_2014_ver1.pdf','facu',NULL,'2015-04-09 09:56:30',3500,'/soc/member_list.pdf',26);
/*!40000 ALTER TABLE `soc_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-25 20:59:12
